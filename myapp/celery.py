import os
from celery import Celery



# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'simpletask.settings')

# Create default Celery app
app = Celery()

# namespace='CELERY' means all celery-related configuration keys
# should be uppercased and have a `CELERY_` prefix in Django settings.
# https://docs.celeryproject.org/en/stable/userguide/configuration.html
app.config_from_object("django.conf:settings", namespace="CELERY")

# When we use the following in Django, it loads all the <appname>.tasks
# files and registers any tasks it finds in them. We can import the
# tasks files some other way if we prefer.
app.autodiscover_tasks()


# app.conf.beat_schedule = {
#     #Scheduler Name
#     'add-five-seconds': {
#         # Task Name (Name Specified in Decorator)
#         'task': 'add',  
#         # Schedule      
#         'schedule': 5.0,
#         # Function Arguments 
#         'args': (0, 3) 
#     },
#     'substract-ten-seconds': {
#         'task': 'substract',  
#         'schedule': 10.0, 
#         'args': (9, 2) 
#     },
#     'multiply-fifteen-seconds': {
#         'task': 'multiply',  
#         'schedule': 15.0,
#         'args': (5, 1) 
#     },
#     'divide-twenty-seconds': {
#         'task': 'divide',
#         'schedule': 20.0,
#         'args': (81, 9)
#     }
# }  

# app.conf.beat_schedule = {
#     #Scheduler Name
#     'execute-five-seconds': {
#         # Task Name (Name Specified in Decorator)
#         'task': 'run-chain',  
#         # Schedule      
#         'schedule': 5.0,
#         # Function Arguments 
#         'args': (2, 3) 
#     }
# }  

app.conf.beat_schedule = {
    #Scheduler Name
    'execute-five-seconds': {
        # Task Name (Name Specified in Decorator)
        'task': 'run-chain',  
        # Schedule      
        'schedule': 30.0,
        # Function Arguments 
        'args': (
            'welcome',
            'Hey, how is it going?',
            'my name is Purple Alien',
            'I come from Andromeda'
        ) 
    }
}  

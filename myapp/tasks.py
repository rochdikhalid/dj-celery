from celery import chain, signature
from celery import shared_task
import time


@shared_task(name = 'state-name')
def get_state(state):
    time.sleep(3)
    return state


@shared_task(name = 'greeting')
def get_greeting(state, message):
    time.sleep(3)
    output = '=== ' + state + ': ' + message + ' ==='
    return output


@shared_task(name = 'get-identity')
def get_identity(state, identity):
    time.sleep(5)
    output = '=== ' + state + ': '+ identity + ' ==='
    return output


@shared_task(name = 'get-location')
def get_location(state, location):
    time.sleep(2)
    output = '=== ' + state + ': ' + location + ' ==='
    return output


@shared_task(name = 'run-chain')
def run_chain(state, message, identity, location):
    canvas = chain(
        get_state.si(state),
        get_greeting.si(state, message),
        get_identity.si(state, identity),
        get_location.si(state, location)
    ).apply_async(countdown = 3) # delay the chain not every task in the chain
    return canvas


# @shared_task(name = 'greeting')
# def get_greeting(state, message):
#     output = '=== ' + state + ': ' + message + ' ==='
#     return output


# @shared_task(name = 'get-identity')
# def get_identity(state, identity):
#     output = '=== ' + state + ': '+ identity + ' ==='
#     return output


# @shared_task(name = 'get-location')
# def get_location(state, location):
#     output = '=== ' + state + ': ' + location + ' ==='
#     return output


# @shared_task(name = 'run-chain')
# def run_chain(state, message, identity, location):
#     canvas = chain(
#         get_state.s(state),
#         get_greeting.s(message),
#         get_identity.s(identity),
#         get_location.s(location)
#     ).apply_async()
#     return canvas

# @shared_task(name = 'add')
# def add(x, y):
#     return x + y


# @shared_task(name = 'substract')
# def substract(x, y):
#     return x - y


# @shared_task(name = 'multiply')
# def multiply(x, y):
#     return x * y


# @shared_task(name = 'divide')
# def divide(x, y):
#     return x / y


# @shared_task(name = 'run-chain')
# def run_chain(x, y):
#     canvas = chain(
#         add.s(x, y),
#         substract.s(y),
#         multiply.s(y),
#         divide.s(y)
#     ).apply_async()
#     return canvas















# canvas.delay()

# canvas = chain(
#     add.si(3, 2).apply_async(countdown = 3),
#     substract.si(3, 2).apply_async(countdown = 3),
#     multiply.si(3, 2).apply_async(countdown = 3),
#     divide.si(3, 2).apply_async(countdown = 3)
# )

# canvas.delay()

# @shared_task(name = 'result')
# def get_chain(out):
#     res_chain = chain(
#         add.s().apply_async(countdown = 3),
#         substract.s().apply_async(countdown = 5),
#         multiply.s().apply_async(countdown = 7),
#         multiply.s().apply_async(countdown = 9) 
#     )

#     res_chain = chain(
#         send_user_message.s().apply_async(countdown=3),
#         send_chat_message.s().apply_async(countdown=3),
#         send_triggers.s().apply_async(countdown=3)
#     )


# result = add.delay(2, 2)

# if result.ready():
#     print('Task has run')
#     if result.successful():
#         print('Result was: %s' % result.result)
#     else:
#         if isinstance(result.result, Exception):
#             print('Task failed due to raising an exception')
#             raise result.result
#         else:
#             print('Task failed without raising exception')
# else:
#     print('Task has not yet run')